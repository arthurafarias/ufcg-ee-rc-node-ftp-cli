const Socket = require("net").Socket
const EventEmitter = require('events');
const path = require('path');
const fs = require('fs');
var stream = require('stream');
var util = require('util');

var ftp = new EventEmitter();

ftp.on("debug", (debug) => { console.log(debug.message); });

ftp.channel = {};
ftp.cmd = {};

ftp.channel.cntrl = new Socket();
ftp.channel.data = new Socket();

ftp.channel.cntrl.connected = false;
ftp.channel.cntrl.host = null;
ftp.channel.cntrl.port = null;

ftp.channel.cntrl.on("error", (err) => {ftp.emit("error", err); ftp.channel.data.removeAllListeners(); ftp.channel.cntrl.removeAllListeners(); });
ftp.channel.data.on("error", (err) => {ftp.emit("error", err); ftp.channel.data.removeAllListeners(); ftp.channel.cntrl.removeAllListeners(); });

ftp.ex = function(cmd, args) {

    try {
        ftp.cmd[cmd](args);
    } catch (e) {
        ftp.emit("error", e);
    }

}

var ftp_server_current_working_directory = null;
var ftp_local_current_working_directory = path.resolve("./");

//ftp.on("get-local-current-directory-succeed", function (local_path) { ftp_local_current_working_directory = local_path; });

ftp.status = {
    POSITIVE_PRELIMINARY_REPLY: "1",
    POSITIVE_COMPLETION_REPLY: "2",
    POSITIVE_INTERMEDIATE_REPLY: "3",
    TRANSIENT_NEGATIVE_COMPLETION_REPLY: "4",
    PERMANENT_NEGATIVE_COMPLETION_REPLY: "5",
    PROTECTED_REPLY: "6"
}

ftp.channel.cntrl.parse_response = function(chunk) {

    // console.log(chunk);
    // console.log(chunk.toString());

    chunk_parsed = chunk.toString().match("([0-9]{1,1})([0-9]{1,1})([0-9]{1,1})([0-9]{0,1})([ -]{1,1})(.*)\r\n");
    response = {
        status: chunk_parsed[1],
        message: chunk_parsed[6]
    }
    return response
}

ftp.cmd.connect = function(args) {

    host = args.host;
    port = args.port;
    user = args.user;
    password = args.password;

    if (ftp.channel.cntrl.connected) {
        ftp.emit("connection-failed", new Error("Already connected!"));
        return
    }

    if (typeof(host) == "undefined")
        host = "localhost"
    if (typeof(port) == "undefined")
        port = 21
    if (typeof(user) == "undefined")
        user = "anonymous"
    if (typeof(password) == "undefined")
        password = null

    ftp.channel.cntrl.once("data", function(chunk) {
        response = ftp.channel.cntrl.parse_response(chunk);
        if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY)
            ftp.emit("connected");
        else
            ftp.emit("connection-failed", reponse);
    });

    ftp.once("connected", function() {

        ftp.channel.cntrl.once("data", function(chunk) {
            response = ftp.channel.cntrl.parse_response(chunk);
            if (response.status == ftp.status.POSITIVE_INTERMEDIATE_REPLY)
                ftp.emit("password-required");
            else if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY)
                ftp.emit("connection-succeed", {
                    host: host,
                    port: port
                });
            else
                ftp.emit("connection-failed", new Error(response.message));
        });

        ftp.channel.cntrl.send_command("USER " + user)

    });

    ftp.once("password-required", function() {
        ftp.channel.cntrl.once("data", function(chunk) {
            response = ftp.channel.cntrl.parse_response(chunk);
            if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY) {
                ftp.emit("connection-succeed", {
                    host: host,
                    port: port
                });
            } else {
                ftp.emit("connection-failed", new Error(response.message));
            }
        });
        ftp.channel.cntrl.send_command("PASS " + password)
    });

    ftp.once("connection-succeed", function() {

        ftp.channel.cntrl.host = host;
        ftp.channel.cntrl.port = port;
        ftp.channel.cntrl.connected = true;

        ftp.once("get-server-current-directory-succeed", function(directory) {
            ftp_server_current_working_directory = directory;
        });

        ftp.cmd.psd();

        ftp.removeAllListeners("connected");
        ftp.removeAllListeners("password-required");
        ftp.removeAllListeners("connection-failed");
    });

    ftp.once("connection-failed", function() {
        ftp.removeAllListeners("connected");
        ftp.removeAllListeners("password-required");
        ftp.removeAllListeners("connection-succeed");
    });

    ftp.channel.cntrl.connect({
        host: host,
        port: port
    }, function(err, data) {});
}

ftp.cmd.disconnect = function() {

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    ftp.channel.cntrl.destroy();

    ftp.channel.cntrl.connected = false;
    ftp.channel.cntrl.host = null;
    ftp.channel.cntrl.port = null;

    ftp.emit("disconnect-succeed");

}

ftp.channel.cntrl.send_command = function(cmd) {

    //console.log(cmd);

    return ftp.channel.cntrl.write(cmd + "\r\n");
}

ftp.cmd.sf = function(args) {

    local_path = args.local_path;

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    if (typeof(args.server_path) == "undefined")
        if (server_path.endsWith("/"))
            server_path = ftp_server_current_working_directory + path.basename(args.local_path);
        else
            server_path = ftp_server_current_working_directory + "/" + path.basename(args.local_path);
    else
        server_path = args.server_path.toString();

    ftp.channel.cntrl.once("data", function(chunk) {

        //console.log(chunk);
        //console.log(chunk.toString());

        response = ftp.channel.cntrl.parse_response(chunk);

        if (response.status != ftp.status.POSITIVE_COMPLETION_REPLY) {
            ftp.emit("error", new Error(response.message));
            return;
        }

        var pasv_addr = ftp.channel.cntrl.parse_response_pasv(response);

        ftp.channel.cntrl.removeAllListeners("data");

        ftp.channel.cntrl.emit("pasv-succeed", pasv_addr);

    });

    ftp.channel.cntrl.once("pasv-succeed", function(addr) {

        //console.log(addr);

        ftp.channel.data = new Socket();
        ftp.channel.data.connect(addr);

        chunk_buffer = [];

        ftp.channel.cntrl.on("data", function(chunk) {

            //console.log(chunk);
            //console.log(chunk.toString());

            response = ftp.channel.cntrl.parse_response(chunk);

            if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY )
            {
                ftp.emit("send-file-succeed", response );
            } else if (response.status == ftp.status.POSITIVE_PRELIMINARY_REPLY) {
                fd = fs.openSync(local_path, "r");
                data = fs.readFileSync(local_path);
                ftp.channel.data.write(data);
                ftp.channel.data.destroy();
            } else {
                ftp.emit("error", new Error(response.message) );
                ftp.channel.data.destroy();
            }
        });

        ftp.channel.cntrl.send_command("STOR " + server_path);

    });

    ftp.channel.cntrl.send_command("PASV");



}

ftp.cmd.gf = function(args) {

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    if (typeof(args.local_path) == 'undefined')
        if (!ftp_local_current_working_directory.endsWith("/"))
            local_path = ftp_local_current_working_directory + "/" + path.basename(args.server_path.toString());
        else
            local_path = ftp_local_current_working_directory + path.basename(args.server_path.toString());

    else
    {
        local_path = args.local_path.toString();

        if (!local_path.startsWith("/"))
        {
            local_path = ftp_local_current_working_directory + "/" + local_path;
        }
    }

    if (!args.server_path.toString().startsWith("/"))
    {
        if (ftp_server_current_working_directory.endsWith("/")){
            server_path = ftp_server_current_working_directory + args.server_path.toString();
        } else {
            server_path = ftp_server_current_working_directory + "/" + args.server_path.toString();
        }
    } else {
        server_path = server_path.toString();
    }

    ftp.channel.cntrl.once("data", function(chunk) {

        //console.log(chunk);
        //console.log(chunk.toString());

        response = ftp.channel.cntrl.parse_response(chunk);

        if (response.status == ftp.status.PERMANENT_NEGATIVE_COMPLETION_REPLY) {
            ftp.emit("error", new Error(response.message));
            return;
        }

        var pasv_addr = ftp.channel.cntrl.parse_response_pasv(response);

        ftp.channel.cntrl.emit("pasv-succeed", pasv_addr);

    });

    ftp.channel.cntrl.once("pasv-succeed", function(addr) {

        //console.log(addr);

        ftp.channel.data = new Socket();

        if (!fs.existsSync(path.dirname(path.resolve(local_path))))
        {
            ftp.emit("error", new Error("Invalid local path."));
            return;
        }

        if (fs.existsSync(local_path)) {
            if (fs.lstatSync(local_path).isDirectory()) {
                ftp.emit("error", new Error("Invalid local path."));
                return;
            }
        }

        outstream = fs.createWriteStream(local_path);

        ftp.channel.data.once("error", (err) => {
            ftp.channel.data.removeAllListeners();
            ftp.emit("error", err);
        });
        ftp.channel.data.pipe(outstream);
        ftp.channel.data.connect(addr);

        chunk_buffer = [];

        ftp.channel.cntrl.on("data", function(chunk) {

            //console.log(chunk);
            //console.log(chunk.toString());

            response = ftp.channel.cntrl.parse_response(chunk);

            if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY )
            {
                ftp.emit("get-file-succeed", response );
            } else if (response.status == ftp.status.POSITIVE_PRELIMINARY_REPLY) {
            } else {
                ftp.emit("get-file-failed", new Error(response.message) );
            }
        });

        ftp.channel.cntrl.send_command("RETR " + server_path);

    });

    ftp.channel.cntrl.send_command("PASV");

    return;
}

ftp.cmd.cld = function(args) {

    target_path = path.resolve(ftp_local_current_working_directory + "/" + args.local_path);

    if (!fs.existsSync(target_path))
    {
        ftp.emit("error", new Error("Directory not found!"));
        return;
    }


    if (!fs.lstatSync(target_path).isDirectory()) {
        ftp.emit("error", new Error("Is not a directory!"));
        return;
    }

    ftp_local_current_working_directory = target_path;
    ftp.emit("change-local-directory-suceed", ftp_local_current_working_directory);

}

ftp.cmd.csd = function(args) {

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    if (typeof(args) == "undefined") {
        ftp.emit("error", new Error("Undefined remote path."));
        return;
    }

    if (typeof(args.server_path) == 'undefined')
        server_path = ftp_server_current_working_directory;
    else
        server_path = args.server_path;

    if (!server_path.startsWith("/"))
    {
        if (ftp_server_current_working_directory.endsWith("/"))
            server_path = ftp_server_current_working_directory + server_path;
        else {
            server_path = ftp_server_current_working_directory + "/" + server_path;
        }
    }

    ftp.channel.cntrl.once("data", function(chunk) {
        response = ftp.channel.cntrl.parse_response(chunk);



        if (response.status == ftp.status.POSITIVE_COMPLETION_REPLY) {

            ftp.once("get-server-current-directory-succeed", (path) => {
                ftp_server_current_working_directory = path;
                ftp.emit("change-server-directory-succeed", {
                    current_directory: path
                });
            });

            ftp.cmd.psd();

            return;
        } else {
            ftp.emit("change-server-directory-failed", new Error(response.message));
            return;
        }
    });

    ftp.channel.cntrl.send_command("CWD " + server_path);

}

ftp.cmd.lld = function(args) {



    if (typeof(args.local_path) == 'undefined')
        local_path = ftp_local_current_working_directory;
    else
        local_path = args.local_path;

    if (!local_path.startsWith("/"))
    {
        local_path = ftp_local_current_working_directory + "/" + local_path;
    }

    ftp.emit("debug", { message: "Local Path: " + local_path });

    const exec = require('child_process').exec;
    exec('ls -la ' + `"${path.resolve(local_path)}"`, (error, stdout, stderr) => {
        if (error) {
            ftp.emit("error", new Error(error));
            return;
        }
        ftp.emit("list-local-directory-succeed", stdout);
    });
}

ftp.cmd.lsd = function(args) {

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    if (typeof(args.server_path) == 'undefined')
        server_path = ftp_server_current_working_directory;
    else
        server_path = args.server_path;

    if (!server_path.startsWith("/"))
    {

        if ( !ftp_server_current_working_directory.endsWith("/") ) {
            server_path = ftp_server_current_working_directory + "/" + server_path;
        } else {
            server_path = ftp_server_current_working_directory + server_path;
        }
    }

    var stat_response = {};

    ftp.channel.cntrl.on("data", function(chunk) {

      //console.log(chunk);
      //console.log(chunk.toString());

        var chunk_splitted = chunk.toString().split("\r\n");

        chunk_splitted.forEach(function(item, index) {

            var item_parsed = item.match("^([0-9]{1,1})([0-9]{1,1})([0-9]{1,1})([0-9]{0,1})([ -]{1,1})(.*)");

            if (item_parsed != null) {
                if (item_parsed[5] == "-") {
                    stat_response = {
                        status: item_parsed[1],
                        message: ""
                    }
                } else if (item_parsed[5] == " ") {
                    if (typeof(stat_response.status) == "undefined") {
                        stat_response = {
                            status: item_parsed[1],
                            message: item_parsed[6]
                        };
                    }
                    ftp.emit("stat-parsed", stat_response);
                }
            } else if (item != "") {
                stat_response.message += item + "\r\n";
            }
        });
    });

    ftp.once("stat-parsed", function (stat_response) {

        if ( stat_response.status == ftp.status.POSITIVE_COMPLETION_REPLY ) {
            ftp.emit("list-server-directory-succeed", stat_response.message);
        } else {
            ftp.emit("error", new Error(stat_response.message) );
        }
    });

    ftp.channel.cntrl.send_command("STAT " + server_path);
}

ftp.cmd.pld = function() {
    ftp.emit("get-local-current-directory-succeed", ftp_local_current_working_directory);
}

ftp.cmd.psd = function() {

    if (!ftp.channel.cntrl.connected) {
        ftp.emit("error", new Error("Must connect first!"));
        return;
    }

    ftp.channel.cntrl.once("data", function(chunk) {
        response = ftp.channel.cntrl.parse_response(chunk);
        server_path = response.message.match("\"(.*)\"")[1];
        ftp.emit("get-server-current-directory-succeed", server_path);
    });

    ftp.channel.cntrl.send_command("PWD");
}

ftp.channel.cntrl.parse_response_pasv = function(response) {
    raw_addr = response.message.match("\(([0-9]+),([0-9]+),([0-9]+),([0-9]+),([0-9]+),([0-9]+)\)");
    addr = {
        host: `${raw_addr[2]}.${raw_addr[3]}.${raw_addr[4]}.${raw_addr[5]}`,
        port: parseInt(raw_addr[6]) * 256 + parseInt(raw_addr[7])
    };
    return addr;
}

module.exports = ftp;
