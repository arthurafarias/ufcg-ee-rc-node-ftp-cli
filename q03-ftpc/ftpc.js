const vorpal = require("vorpal")();
const ftp = require("./ftp.js");

vorpal
    .command(
        "connect <host> [port]",
        "Connect to specified remote <host> and [port]. if you does not specify [port] it will be used default 21 port"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.on("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.on("connection-succeed", function(conn) {
                parent.log(`Connected with "${conn.host}" at port "${conn.port}"`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.on("connection-failed", function(err) {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.ex("connect", {
                host: args.host,
                port: args.port,
                user: args.options.user,
                password: args.options.password
            });

        }
    )
    .option('-u, --user <account>', "Specify user's account to log in")
    .option('-p, --password <plain>', "Specify plain user's password");

vorpal
    .command(
        "disconnect",
        "Disconnect from current connection"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.once("disconnect-succeed", function () {
                parent.log("Disconnected!");
                ftp.removeAllListeners();
                callback();
            });

            ftp.ex("disconnect", {});
        }
    )

vorpal
    .command(
        "gf <server_path> [local_path]",
        "Get file from remote FTP server"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.on("get-file-failed", function(err) {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.on("get-file-succeed", function(res) {
                parent.log(`Success: ${res.message}`);
                ftp.removeAllListeners();
                callback();
            });

            parent.log(args);

            ftp.ex("gf", args);
        }
    );

vorpal
    .command(
        "sf <local_path> [server_path]",
        "Send file to remote FTP server"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.on("send-file-succeed", function(res) {
                parent.log(`Success: ${res.message}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.ex("sf", {
                server_path: args.server_path,
                local_path: args.local_path
            });
        }
    );

vorpal
    .command(
        "csd <server_path>",
        "Change server directory specified by <server_path>"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });
            ftp.once("change-server-directory-failed", function(err) {
                parent.log("Error: " + err);
                ftp.removeAllListeners();
                callback();
            });
            ftp.once("change-server-directory-succeed", function(args) {
                parent.log("Current Directory: " + args.current_directory);
                ftp.removeAllListeners();
                callback();
            });

            ftp.ex("csd", {
                server_path: args.server_path
            });
        }
    );

vorpal
    .command(
        "cld <local_path>",
        "Change local directory specified by <local_path>"
    )
    .action(

        function(args, callback) {

            var parent = this;

            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.once("change-local-directory-suceed", function(path) {
                parent.log(path);
                ftp.removeAllListeners();
                callback();
            });
            ftp.ex("cld", args);

        }
    );

vorpal
    .command(
        "lsd [server_path]",
        "List local directory specified by [server_path]. If you want to list current server directory, just does not specify any path or ./"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });

            ftp.once("list-server-directory-succeed", (list) => {
                parent.log(list);
                ftp.removeAllListeners();
                callback();
            })
            ftp.ex("lsd", {
                server_path: args.server_path
            });
        }
    );

vorpal
    .command(
        "lld [server_path]",
        "List local directory specified by [local_path]. If you want to list current local directory, just does not specify any path or ./"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });
            ftp.once("list-local-directory-succeed", (list) => {
                parent.log(list);
                ftp.removeAllListeners();
                callback();
            });
            ftp.ex("lld", {
                server_path: args.server_path
            });
        }
    );

vorpal
    .command(
        "psd",
        "Print server directory"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });
            ftp.once("get-server-current-directory-succeed", function(server_path) {
                parent.log(server_path);
                ftp.removeAllListeners();
                callback();
            });
            ftp.ex("psd", {});
        }
    );

vorpal
    .command(
        "pld",
        "Print local directory"
    )
    .action(

        function(args, callback) {
            var parent = this;
            ftp.once("error", (err) => {
                parent.log(`${err}`);
                ftp.removeAllListeners();
                callback();
            });
            ftp.once("get-local-current-directory-succeed", function(local_path) {
                parent.log(local_path);
                callback();
            });
            ftp.ex("pld", {});

        }
    );

vorpal
    .delimiter('ftpc > ')
    .show();
